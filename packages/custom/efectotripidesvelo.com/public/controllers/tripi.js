'use strict';

import 'web-animations-js/web-animations.min';
import 'hammerjs/hammer';

import 'core-js/es6/symbol';
import 'core-js/es6/object';
import 'core-js/es6/function';
import 'core-js/es6/parse-int';
import 'core-js/es6/parse-float';
import 'core-js/es6/number';
import 'core-js/es6/math';
import 'core-js/es6/string';
import 'core-js/es6/date';
import 'core-js/es6/array';
import 'core-js/es6/regexp';
import 'core-js/es6/map';
import 'core-js/es6/set';

import 'web-animations-js';
import 'angular-touch';


angular.module('mean.system').controller('TripiController', ['$scope', 'Global', '$sce',
  function ($scope, Global, $sce) {
   //<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/VKILa5Ra8O0?rel=0&amp;showinfo=0&amp;start=30" frameborder="0" allowfullscreen></iframe>
   //<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/ib9RrJwOGnk?rel=0&amp;showinfo=0&amp;start=30" frameborder="0" allowfullscreen></iframe>
    $scope.tripiVideoLista = [0];
    $scope.tripiYTids = [
      'VKILa5Ra8O0',
      'ib9RrJwOGnk',
      'iGM1gbMZC30',
      '2adUdJK8_14',
      'N6ogHvW70W4',
      'nh3maKQv2v0',
      '8Nm9goD9Jao',
      'TdGB8f1cVs4',
    ];

    $scope.tripiVideos = [
        {
          'src': 'https://www.youtube.com/embed/VKILa5Ra8O0?rel=0&amp;showinfo=0&amp;enablejsapi=1&amp;start=30',
          'desc': 'video en academia de san carlos cdmx'
        },
        {
          'src': 'https://www.youtube.com/embed/ib9RrJwOGnk?rel=0&amp;showinfo=0&amp;enablejsapi=1&amp;start=30',
          'desc': 'video en academia de san carlos cdmx'
        },
        {
          'src': 'https://www.youtube.com/embed/iGM1gbMZC30?rel=0&amp;showinfo=0&amp;enablejsapi=1&amp;start=30',
          'desc': 'video en academia de san carlos cdmx'
        },
        {
          'src': 'https://www.youtube.com/embed/2adUdJK8_14?rel=0&amp;showinfo=0&amp;enablejsapi=1&amp;start=30',
          'desc': 'video en academia de san carlos cdmx'
        },
        {
          'src': 'https://www.youtube.com/embed/N6ogHvW70W4?rel=0&amp;showinfo=0&amp;enablejsapi=1&amp;start=30',
          'desc': 'video en academia de san carlos cdmx'
        },
        {
          'src': 'https://www.youtube.com/embed/nh3maKQv2v0?rel=0&amp;showinfo=0&amp;enablejsapi=1&amp;start=30',
          'desc': 'video en academia de san carlos cdmx'
        },
        {
          'src': 'https://www.youtube.com/embed/8Nm9goD9Jao?rel=0&amp;showinfo=0&amp;enablejsapi=1&amp;start=30',
          'desc': 'video en academia de san carlos cdmx'
        },
        {
          'src': 'https://www.youtube.com/embed/TdGB8f1cVs4?rel=0&amp;showinfo=0&amp;enablejsapi=1&amp;start=30',
          'desc': 'video en academia de san carlos cdmx'
        },
        {
          'src': 'https://www.youtube.com/embed/xdw65FHvG08?rel=0&amp;showinfo=0&amp;enablejsapi=1&amp;start=30',
          'desc': 'video en academia de san carlos cdmx'
        },

    ];

    $scope.tripiPhotosAll = [
      'meanStarter/assets/img/efecto01/album03/DSC08530.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08535.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08536.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08539.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08542.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08546.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08547.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08552.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08556.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08573.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08591.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08593.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08594.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08595.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08596.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08597.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08598.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08599.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08600.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08601.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08602.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08603.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08604.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08605.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08606.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08607.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08608.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08609.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08610.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08611.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08612.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08613.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08614.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08772.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08773.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08774.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08775.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08776.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08777.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08820.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08821.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08822.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08823.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08824.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08825.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08826.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08827.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08828.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08829.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08830.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08831.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08832.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08833.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08834.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08835.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08836.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08837.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08838.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08839.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08840.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08841.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08842.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08843.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08844.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08845.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08846.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08847.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08848.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08849.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08850.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08851.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08852.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08853.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08854.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08855.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08856.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08857.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08858.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08859.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08860.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08861.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08862.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08863.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08864.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08865.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08866.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08867.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08868.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08869.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08870.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08871.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08872.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08873.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08874.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08875.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08876.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08877.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08879.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08891.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08897.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08898.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album03/DSC08899.jpg_cardsize.jpeg',
      'meanStarter/assets/img/efecto01/album02/DSC02600.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02601.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02602.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02603.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02604.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02605.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02606.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02607.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02608.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02609.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02610.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02611.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02612.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02613.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02614.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02615.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02616.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02617.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02618.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02619.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02620.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02621.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02622.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02623.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02624.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02625.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02626.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02627.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02628.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02629.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02630.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02631.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02632.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02633.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02634.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02635.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02636.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02637.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02638.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02639.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02640.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02641.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02642.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02643.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02645.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02647.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02652.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02654.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02655.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02656.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02657.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02658.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02659.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02660.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02661.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02662.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02663.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02664.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02665.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02666.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02668.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02669.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02670.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02672.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02674.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02675.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02676.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02677.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02678.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02679.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02680.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02681.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02690.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02696.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02699.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02700.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02701.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02702.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02703.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02704.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02708.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02709.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02712.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02713.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02714.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02715.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02716.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02717.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02718.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02719.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02720.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02721.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02722.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02723.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02724.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02725.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02726.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02727.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02728.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02729.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album02/DSC02730.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album01/DSC01540.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album01/DSC01543.JPG_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album01/DSC01544.JPG_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album01/DSC01559.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album01/DSC01561.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album01/DSC01562.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album01/DSC01563.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album01/DSC01565.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album01/DSC01566.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album01/DSC01567.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album01/DSC01568.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album01/DSC01569.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album01/DSC01570.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album01/DSC01571.jpg_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album01/DSC01573.JPG_cardsize.jpeg',
'meanStarter/assets/img/efecto01/album01/DSC01588.JPG_cardsize.jpeg',
    ];

  $scope.tripiEntries = [
    {
    'title': 'Efecto Tripidesvelo se interna en la mente de la audiencia con sonidos y video mapeo, los resultados son inesperados',
    'subhead':'¿Efecto Tripidesvelo?',
    'text': 'La manifestación de los sonidos siempre presentes en lo cotidiano escapan de nuestra atención para alojarse en lo más profundo del inconsciente; una realidad que se accede justo antes de caer profuntamente dormido, que intensificado por un previo y largo periodo de insomnio nos va sumergiendo en ese infinito almacen de sensaciones, recuerdos y memorias. Efecto Tripidesvelo explora, investiga y desarrolla patrones audiovisulaes para crear objetos sonoros capaces de generar poderosas imagenes mentales y sensoriales que cada espectador descubre y da significado por cuenta propia. El arte sonoro, audiovisual y multimedia de Efecto Tripidesvelo es alimentado por una constante investigación, desarrollo de obras y el uso de nuevas tecnologias de forma metódica y multidisciplinaria.',
    'author': 'eccker',
    'link': 'https://efectotripidesvelo.com',
    'image': '/meanStarter/assets/img/efecto01/album02/DSC02730.jpg_cardsize.jpeg',
    'avatar': '/meanStarter/assets/img/efecto01/efecto_logo_s.png',
    'user': 'eccker_etd',
    'phrase': 'ecckertechnologies.com',
    'ranking': 1,
    'first_publish_date': '',
    'last_publish_update': '',
    'likes': 0,
    'index': 0, 'yt_video_ids': $scope.tripiYTids[getRandomInt(0, $scope.tripiYTids.length - 1)],
    'photos' : [
      {
        'src': '/meanStarter/assets/img/efecto01/album02/DSC02716.jpg_cardsize.jpeg',
        'desc': 'Image01'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album02/DSC02610.jpg_cardsize.jpeg',
        'desc': 'Image02'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album02/DSC02613.jpg_cardsize.jpeg',
        'desc': 'Image03'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album02/DSC02679.jpg_cardsize.jpeg',
        'desc': 'Image04'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album03/DSC08826.jpg_cardsize.jpeg',
        'desc': 'Image05'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album03/DSC08539.jpg_cardsize.jpeg',
        'desc': 'Image06'
      }
    ]
  },
  {
    'title': 'Otorgan mencion honorifica a Efecto Tripidesvelo en Bienal de Radio Mexicana ',
    'subhead':'Interesante narativa de ...',
    'text': 'Ceremonia en CENART en causa de la premiación de la convocatoria a la ??? Bienal de Radio.',
    'author': 'eccker',
    'link': 'https://efectotripidesvelo.com',
    'image': '/meanStarter/assets/img/efecto01/album03/DSC08879.jpg_cardsize.jpeg',
    'avatar': '/meanStarter/assets/img/efecto01/tripidesvelo_logo_s.png',
    'user': 'tripiado1',
    'phrase': 'tripiando en maquina virtual',
    'ranking': 1,
    'first_publish_date': '',
    'last_publish_update': '',
    'likes': 0,
    'index': 0, 'yt_video_ids': $scope.tripiYTids[getRandomInt(0, $scope.tripiYTids.length - 1)],
    'photos' : [
      {
        'src': '/meanStarter/assets/img/efecto01/album02/DSC02602.jpg_cardsize.jpeg',
        'desc': 'Image07'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album02/DSC02603.jpg_cardsize.jpeg',
        'desc': 'Image08'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album02/DSC02604.jpg_cardsize.jpeg',
        'desc': 'Image09'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album02/DSC02605.jpg_cardsize.jpeg',
        'desc': 'Image10'
      },

      {
        'src': '/meanStarter/assets/img/efecto01/album02/DSC02617.jpg_cardsize.jpeg',
        'desc': 'Image11'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album02/DSC02631.jpg_cardsize.jpeg',
        'desc': 'Image12'
      }

    ]

  },
  {
    'title': 'Efecto Tripidesvelo de Gira por el Valle del Mezquital',
    'subhead':'Cavernas subterraneas, aguas termales, desierto y buena vibra sonora',
    'text': 'El valle del Mezquital, Hidalgo, México',
    'author': 'eccker',
    'link': 'https://efectotripidesvelo.com',
    'image': '/meanStarter/assets/img/efecto01/album01/DSC01540.jpg_cardsize.jpeg',
    'avatar': '/meanStarter/assets/img/efecto01/efectotripidesvelo_logo.png',
    'user': 'tripidesvelado1',
    'phrase': 'Tripidesveleando hasta el amanecer de mañana',
    'ranking': 1,
    'first_publish_date': '',
    'last_publish_update': '',
    'likes': 0,
    'index': 0, 'yt_video_ids': $scope.tripiYTids[getRandomInt(0, $scope.tripiYTids.length - 1)],
    'photos' : [
      {
        'src': '/meanStarter/assets/img/efecto01/album01/DSC01540.jpg_cardsize.jpeg',
        'desc': 'Image13'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album01/DSC01543.JPG_cardsize.jpeg',
        'desc': 'Image14'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album01/DSC01544.JPG_cardsize.jpeg',
        'desc': 'Image15'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album01/DSC01573.JPG_cardsize.jpeg',
        'desc': 'Image16'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album01/DSC01588.JPG_cardsize.jpeg',
        'desc': 'Image17'
      },
    ]
  },
  {
    'title': 'Grutas de Xoxafi son exploradas e intervenidas audiovisualmente por Efecto Tripidesvelo',
    'subhead':'Exploración sonora en el corazón de la tierra.',
    'text': '',
    'author': 'eccker',
    'link': 'https://efectotripidesvelo.com',
    'image': '/meanStarter/assets/img/efecto01/album01/DSC01562.jpg_cardsize.jpeg',
    'avatar': '/meanStarter/assets/img/efecto01/efectotripidesvelo_logo_s2.png',
    'user': 'tripitas',
    'phrase': 'Tripi tripi',
    'ranking': 1,
    'first_publish_date': '',
    'last_publish_update': '',
    'likes': 0,
    'index': 0, 'yt_video_ids': $scope.tripiYTids[getRandomInt(0, $scope.tripiYTids.length - 1)],
    'photos' : [
      {
        'src': '/meanStarter/assets/img/efecto01/album01/DSC01559.jpg_cardsize.jpeg',
        'desc': 'Image18'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album01/DSC01561.jpg_cardsize.jpeg',
        'desc': 'Image19'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album01/DSC01562.jpg_cardsize.jpeg',
        'desc': 'Image20'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album01/DSC01563.jpg_cardsize.jpeg',
        'desc': 'Image21'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album01/DSC01565.jpg_cardsize.jpeg',
        'desc': 'Image22'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album01/DSC01566.jpg_cardsize.jpeg',
        'desc': 'Image23'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album01/DSC01567.jpg_cardsize.jpeg',
        'desc': 'Image24'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album01/DSC01568.jpg_cardsize.jpeg',
        'desc': 'Image25'
      },
      {
        'src': '/meanStarter/assets/img/efecto01/album01/DSC01569.jpg_cardsize.jpeg',
        'desc': 'Image26'
      }

    ]

  },
];

var rg = new RiGrammar();
rg.addRule('<start>', '<pronombre> <sujeto> <verbo>', 1);
rg.addRule('<pronombre>','el | ella | eso', 1);
rg.addRule('<sujeto>','gato | perro | conejo', 1);
rg.addRule('<verbo>','corre | nada | levanta | trae', 4);
rg.addRule('<verbo>','lee | entiende | escucha | comprende', 1);
rg.addRule('<verbo>','corta | rasca | golpea | grita', 6);
resultado = rg.expand()
console.log(resultado);








    /////////////////////////////////////////////

  $scope.add = function () {
        // variable para almacenar el indice de las entradas (tripiEntries)
        var entryIdx = [];
        // agrega 'numeroEntradas' entradas
        var numeroEntradas = 9;
        for (var i = 0; i < numeroEntradas; i++) {
          resultado = rg.expand()
          console.log(resultado);
          $scope._Index.push(0);
          entryIdx.push( getRandomInt(0, $scope.tripiEntries.length-1) );
        	$scope.tripiEntries.push({
            'title': objetoGramatico.flatten('#una voz#') + ' ' +  objetoGramatico.flatten('#que estan haciendo#'),
            'subhead': objetoGramatico.flatten('#percepcion#') + ' y/o ' + objetoGramatico.flatten('#percepcion#'),
            'text': objetoGramatico.flatten('#semilla#'),
            'author': 'eccker',
            'link': 'https://efectotripidesvelo.com',
            'image': '/meanStarter/assets/img/efecto01/album01/DSC01540.jpg_cardsize.jpeg',
            'avatar': $scope.tripiEntries[getRandomInt(0 , $scope.tripiEntries.length - 1)].avatar,
            'user': $scope.tripiEntries[getRandomInt(0 , $scope.tripiEntries.length - 1)].user,
            'phrase': $scope.tripiEntries[getRandomInt(0 , $scope.tripiEntries.length - 1)].phrase,
            'ranking': 1,
            'first_publish_date': '',
            'last_publish_update': '',
            'likes': 0,
            'index': 0, 'yt_video_ids': $scope.tripiYTids[getRandomInt(0, $scope.tripiYTids.length - 1)],
            'photos' : [
              {
                'src': $scope.tripiPhotosAll[getRandomInt(0,$scope.tripiPhotosAll.length - 1)],
                'desc': 'Image01'
              },
              {
                'src': $scope.tripiPhotosAll[getRandomInt(0,$scope.tripiPhotosAll.length - 1)],
                'desc': 'Image02'
              },
              {
                'src': $scope.tripiPhotosAll[getRandomInt(0,$scope.tripiPhotosAll.length - 1)],
                'desc': 'Image03'
              },
              {
                'src': $scope.tripiPhotosAll[getRandomInt(0,$scope.tripiPhotosAll.length - 1)],
                'desc': 'Image04'
              },
              {
                'src': $scope.tripiPhotosAll[getRandomInt(0,$scope.tripiPhotosAll.length - 1)],
                'desc': 'Image05'
              },
            ]
        	});
        }

    };

    function getRandomInt(min, max) {
    			return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    $scope.getRandomInt = function(min, max) {
    			return Math.floor(Math.random() * (max - min + 1)) + min;
    };


    $scope._Index = [0];
    for (var idx = 0; idx < $scope.tripiEntries.length; idx++){
      $scope._Index.push(0);
    }
    // if a current image is the same as requested image
    $scope.isActive = function (index, entry) {
      //var jsonText = JSON.stringify(entry);
      //console.log('THIS IS ' + jsonText);
    return $scope._Index[entry] === index;
    };
    // show prev image
    $scope.showPrev = function (index, entry) {
      //var jsonText = JSON.stringify(entry);
//      generar();
      //console.log('showPrev with param ' + jsonText);
    $scope._Index[entry] = ($scope._Index[entry] > 0) ? --$scope._Index[entry] : $scope.tripiEntries[entry].photos.length - 1;
    };
    // show next image
    $scope.showNext = function (index, entry) {
      //var jsonText = JSON.stringify(entry);
      //console.log('showNext with param ' + jsonText); $scope.tripiYTids
    $scope._Index[entry] = ($scope._Index[entry] < $scope.tripiEntries[entry].photos.length - 1) ? ++$scope._Index[entry] : 0;
    };
    // show a certain image
    $scope.showPhoto = function (index, entry) {
      //var jsonText = JSON.stringify(entry);
      //console.log('showPhoto with param ' + jsonText);
    $scope._Index[entry] = index;
    };



    function getVideoURL()
      {

        var resultado = $scope.tripiVideos[getRandomInt(0, $scope.tripiVideos.length - 1)].src.replace('\'', '');
        console.log('---------------' + resultado);
        return resultado;
      }

    $scope.currentMedia = [
      {'url': $sce.trustAsResourceUrl(getVideoURL()),
      'ytpid':'0'}
    ];

$scope.genOneUniqueRandomVideoURL = function()
  {
    $scope.getRandomVideoURL();
    return new Array(1);
  };



var number = 65;
    $scope.getRandomVideoURL = function() {
        $scope.currentMedia.url = $sce.trustAsResourceUrl(getVideoURL());
        $scope.currentMedia.ytpid= String.fromCharCode(number);
        number++;

        console.log('currentMedia.url es: ' + $scope.currentMedia.url) ;
        console.log('currentMedia.ytpid es: ' + $scope.currentMedia.ytpid) ;
        return $scope.currentMedia.url;
    }



    //////////////////////
    $scope.global = Global;
  }
])
.config(['$qProvider', function ($qProvider) {
  $qProvider.errorOnUnhandledRejections(false);
} ]).config(function($sceDelegateProvider) {
 $sceDelegateProvider.resourceUrlWhitelist([
   // Allow same origin resource loads.
   'self',
   // Allow loading from our assets domain.  Notice the difference between * and **.
   'https://www.youtube.com/**']);
 });




////
